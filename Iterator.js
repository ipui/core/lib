class Iterator {

  static async reduce( iterator, transform ) {
    const item = await iterator.next()
    if ( item.done ) return []

    return [ transform( item.value ), ...( await Iterator.reduce( iterator, transform ) ) ]
  }

}

export default Iterator
