function copyToClipboard( text ) {
  try {
    navigator.clipboard.writeText( text );
  } catch ( err ) {
    let textField = document.createElement( 'textarea' );
    textField.innerText = text;
    document.body.appendChild( textField );
    textField.select();
    document.execCommand( 'copy' );
    textField.remove();
  }
}

export default copyToClipboard;
